<?php
/*
Template Name: Servies Page
*/
?>

<?php get_header('header-custom.php'); ?>


<div class="custom-page services-page row-fluid">

	<div class="span6">
		<h1><?php the_title(); ?></a></h1>
		<?php query_posts('category_name=neuroimaging services&order=ASC'); ?>
		<?php while (have_posts()) : the_post(); ?>
		<h2><?php the_title(); ?> </h2>
		<p><?php the_content(); ?></p>
		<?php endwhile;?>
		<div class="services-contact">
			<p><a href="/contact/">Contact us</a> for more information.</p>
		</div>
	</div>

	<div class="span2"></div>

<div class="span4 sidebar-2">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-2') ) ?>	
</div>

</div>

<?php get_footer(); ?>