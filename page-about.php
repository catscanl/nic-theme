<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>


<div class="custom-page about-page row-fluid">
	<div class="span6">
	<h1><?php the_title(); ?></a></h1>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	    	<?php the_content(); ?>

		<?php endwhile; else: ?> 

	    	<p><?php _e('Sorry, this page does not exist.'); ?></p>
	    <?php endif; ?>

		<div class="services-contact">
			<p><a href="/contact/">Contact us</a> for more information.</p>
		</div>
	</div>

	<div class="span2"></div>

	<div class="span4 sidebar-2">
	 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-2') ) ?>	
	</div>

</div>


<?php get_footer(); ?>