<?php
/*
Template Name: News Page
*/
?>

<?php get_header(); ?>
<div class="custom-page row-fluid">

	 <div class="post span7">
	 	<h1><?php the_title(); ?></a></h1>
	 	<?php $query = new WP_Query( 'cat=-7, -8' ); ?>
	 	<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


	 	<!-- Display the Title as a link to the Post's permalink. -->
		 <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

		 <p><?php the_content(); ?><p>
		 <!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->
		 <small><?php the_time( 'F jS, Y' ); ?> by Neuroimaging Consult. </small>
	 
	 	 <small class="postmetadata"><?php _e( 'Posted in' ); ?> <?php the_category( ', ' ); ?></small>


		 <?php endwhile; 
		 wp_reset_postdata();
		 else : ?>
		 <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		 <?php endif; ?>

	</div>
		 
	<div class="span4">
		<?php get_sidebar(); ?>  	
	</div>

</div>

<?php get_footer(); ?>