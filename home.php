<?php get_header(); ?>

<div class="row-fluid banner">
	<div class="span1"> </div>
	
	<div class="span6 banner-writing">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	    	<?php the_content(); ?>

		<?php endwhile; else: ?> 

	    	<p><?php _e('Sorry, this page does not exist.'); ?>
	      
		</p><?php endif; ?>

	</div>

	<div class="span1"> </div>

	<div class="span4 banner-pic">
		<a href="/wp-content/uploads/2015/01/bluesquare.png"><img class="alignnone size-medium wp-image-52" src="http://localhost:8888/wp-content/uploads/2015/01/bluesquare-300x225.png" alt="bluesquare" width="300" height="225" /></a>
	</div>
</div>

<div class="row-fluid">
	<div class="span1"></div>
	<?php
	$num_cols = 3;	
	query_posts('category_name=services-short&order=ASC');
	if (have_posts()) : for ( $i=1 ; $i <= $num_cols; $i++ ) :
    echo '<div id="col-'.$i.'" class="col span3">';
    $counter = $num_cols + 1 - $i;
    while (have_posts()) : the_post();
      if( $counter%$num_cols == 0 ) : ?>
        <h3><a href="/services/" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
        <p><?php the_content(); ?></p>
      <?php endif;
      $counter++;
    endwhile;
    rewind_posts();
    echo '</div>'; //closes the column div
  endfor;
	else:
	  echo 'no posts';
	endif;
	wp_reset_query();
?>


<?php get_footer(); ?>
