<?php
/*
Template Name: NIC Custom Page
*/
?>

<?php get_header(); ?>

<div class="span1"></div>

<div class="custom-page span6">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	    <?php the_content(); ?>

	<?php endwhile; else: ?> 

	    <p><?php _e('Sorry, this page does not exist.'); ?>
	      
	</p><?php endif; ?>

</div>

<?php get_footer(); ?>