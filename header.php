<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php wp_enqueue_script("jquery"); ?>   
    <?php wp_head(); ?>

  </head>

  <body>
  <div id="wrapper">
    <div class="navbar navbar-harvest navbar-static-top">  
      <div class="navbar-inner">         
          <a class="btn btn-navbar btn-harvest" data-toggle="collapse" data-target=".nav-collapse">        
            <span class="icon-bar"></span>        
            <span class="icon-bar"></span>        
            <span class="icon-bar"></span>      
          </a>      
          <a class="brand" href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>     
          <div class="nav-collapse collapse">        
            <ul class="nav">

                <?php wp_list_pages(array('title_li' => '')); ?>
                
            </ul>      
          </div><!--/.nav-collapse -->     
      </div>
  </div>

  <div class="main">