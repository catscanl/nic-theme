<?php get_header(); ?>

<div class="custom-page category-page row-fluid">
	  <div class="span7">
	  	<h1>You searched for: <?php the_search_query(); ?></h1>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		    <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		    <p><em><?php the_time('F jS, Y'); ?></em></p>
		    <p><?php the_content(); ?></p>

		<?php endwhile; else: ?> 

		    <p><?php _e('Sorry, no results. Try again.'); ?></p>
			<p><?php get_search_form(); ?></p>

			<?php endif; ?>
	</div>

	<div class="span4">
		<?php get_sidebar(); ?>  	
	</div>
</div>
   
<?php get_footer(); ?>