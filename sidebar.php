

<div class="sidebar-cat">
	<div class="sidebar-div">
		<h2>Recent Posts</h2>
		<p><?php wp_get_archives('type=postbypost&limit=15&cat=-7,-8'); ?></p>
	</div>

	<div class="sidebar-div">
		<h2>Archives</h2>
		<p><?php wp_get_archives('type=monthly'); ?></p>
	</div>

	<div class="sidebar-div">
		<?php get_search_form(); ?>
	</div>

</div>