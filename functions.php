<?php 

	function wpbootstrap_scripts_with_jquery()
	{	
	// Register the script like this for a theme:	
		wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );	
		// For either a plugin or a theme, you can then enqueue the script:	
		wp_enqueue_script( 'custom-script' );
	}

	add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
        'name' => 'Sidebar 1', 
        'id' => 'sidebar-1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
        'name' => 'Sidebar 2', 
        'id' => 'sidebar-2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));

}

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
 
/* Theme setup */
add_action( 'after_setup_theme', 'wpt_setup' );
    if ( ! function_exists( 'wpt_setup' ) ):
        function wpt_setup() {  
            register_nav_menu( 'primary', __( 'Primary navigation', 'wpnic' ) );
        } endif;

/* move admin bar to footer*/
// function fb_move_admin_bar() {
//     echo '
//     <style type="text/css">
//     body {
//     margin-top: -28px;
//     padding-bottom: 28px;
//     }
//     body.admin-bar #wphead {
//        padding-top: 0;
//     }
//     body.admin-bar #footer {
//        padding-bottom: 28px;
//     }
//     #wpadminbar {
//         top: auto !important;
//         bottom: 0;
//     }
//     #wpadminbar .quicklinks .menupop ul {
//         bottom: 28px;
//     }
//     </style>';
// }
// // on backend area
// add_action( 'admin_head', 'fb_move_admin_bar' );
// // on frontend area
// add_action( 'wp_head', 'fb_move_admin_bar' );

// ?>