<?php get_header(); ?>

<div class="custom-page row-fluid">
	  <div class="span7">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		    <h1><?php the_title(); ?></h1>
		    <p><em><?php the_time('F jS, Y'); ?></em></p>
		    <?php the_content(); ?>

		<?php endwhile; else: ?> 

		    <p><?php _e('Sorry, this post does not exist.'); ?>
		      
			</p><?php endif; ?>
	</div>

	<div class="span4">
		<?php get_sidebar(); ?>  	
	</div>
</div>
   
<?php get_footer(); ?>